= How to use the file edit interface

This section describes how to contribute to existing documentation - that is, documentation that already has been published on the website.
Before you start following this procedure, make sure that you fulfill all the requirements listed in xref:index.adoc[Prerequisites].

[TIP]
If you are interested in contributing to Release Notes, see the xref:contributing-docs/contrib-release-notes.adoc[appropriate page].

Each Fedora Docs page includes an "Edit this Page" link at the top.
For simple updates, changes can be submitted directly from the Pagure web interface.
For larger or more complex edits, you should prepare and test your changes offline before submitting.

== Editing online in Pagure

. Click the "Edit this Page" link to load the documentation source.
  You will be taken to the appropriate content repository in Pagure.

. Above the source listing on the right side, click "Fork and Edit".
  (If you have already forked the repository, this button will be labeled "Edit in your fork" and you can skip to the next step.)

.. If you are not already logged in to Pagure, you will be asked for your credentials.

.. Wait for the operation to finish.
   You may need to refresh the page as it does not always update automatically when the process is done.

. Once the file is loaded in your fork, make any changes necessary to the content, commit them to your fork, and prepare a pull request (PR).

.. Each PR should be submitted from its own branch in your repository.
   Under the "Branch" heading of the commit interface, select "New branch" and give the branch a short, unique name.

.. Fill out the commit message form.

.. Click "Commit changes" to create the branch and save these changes to your fork.

. Once the commit is saved, the page will refresh to a list of Commits for your fork.

. To include additional, related changes, repeat this process and commit them to the same branch.

. When you are ready, click "Create pull request" and fill out the PR form to submit your branch to the upstream repository.

[TIP]
.Git commit tips
====
* When naming your branch, use only ASCII letters, digits, hyphens (`-`) and underscores (`_`).
  The name pass:q[_may_] contain, but not start or end with, single periods (`.`).
Spaces, double periods (`..`), and most other punctuation are not permitted.

* The commit title is how your edit will be identified in the repository log for the page.
  The suggested title, "Update [filename]``pathname``", is sufficient for small edits.
  For some advice on writing good commit messages,
see link:++https://commit.style++[commit.style] by Tim Pope (author of [application]`vim`).

* Use the "Commit Description" field to provide additional detail if necessary, but keep it short.
  You will have the opportunity to explain or discuss your changes when you submit your PR.
====

== Offline editing

. Click the "Edit this Page" link to load the documentation source.
  You will be taken to the appropriate content repository in Pagure.
  Once you have located the correct repository, make a fork if you do not have it forked already:

.. In the top right corner, click Fork.

.. If you are not already logged in to Pagure, you will be asked for your credentials.

.. Wait for the operation to finish.
  You may need to refresh the page as it does not always update automatically when the process is done.

.. Clone your fork.

. Check out a new branch, and add your contributions.

. If you added any new files, then ensure they are included in a reasonable spot in the repository's [filename]`nav.adoc` configuration file

. Build locally and make sure everything looks the way you expect.
  See xref:contributing-docs/tools-localenv-preview.adoc[Building a local preview] for instructions.

. Once you finish, commit your changes and push them to your fork.

. Use Pagure to make a pull request from your fork to the main repository's master branch.

== Managing your pull request

Someone will see your pull request and either merge it, or provide feedback if there is something you should change.
Work with the people commenting to make sure your contributions are up to standards.

[NOTE]
====
If nobody reacts to your pull request in several days, try bringing it up on one of the link:++https://calendar.fedoraproject.org//docs/++[weekly meetings], the Matrix channel (https://matrix.to/#/#docs:fedoraproject.org[#docs] on Fedora Chat), or the link:++https://discussion.fedoraproject.org/tag/docs++[mailing list].
====

Your changes will appear online sometime after the pull request is merged.
The site is being updated daily.
If your changes do not appear online within 60 hours of your PR being merged, ping `asamalik` (Adam Šamalík) on the IRC channel and ask him about it.
